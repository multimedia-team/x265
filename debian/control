Source: x265
Section: video
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Sebastian Ramacher <sramacher@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 git,
 libnuma-dev [amd64 arm64 i386 mips mips64 mips64el mipsel powerpc ppc64el],
 nasm [amd64],
 ninja-build
Build-Depends-Indep:
 dh-sequence-sphinxdoc,
 python3-sphinx,
 python3-sphinx-rtd-theme
Standards-Version: 4.7.0
Homepage: https://bitbucket.org/multicoreware/x265_git
Vcs-Git: https://salsa.debian.org/multimedia-team/x265.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/x265
Rules-Requires-Root: no

Package: x265
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: H.265/HEVC video stream encoder
 x265 is a commandline encoder for creating H.265/High Efficiency Video Coding
 (HEVC) video streams.
 .
 x265 supports the following features:
  * full prediction and transform quad-tree recursion supported
  * adaptive B-frame placement
  * B-frames as references / arbitrary frame order
  * CABAC (context-based adaptive binary arithmetic coding) entropy coding
  * supports all Intra block types
  * supports all Inter P partitions
  * supports all Inter B partitions from 64x64 down to 8x4
  * weighted prediction for P slices
  * multiple reference frames
  * scenecut detection
  * parallel encoding on multiple CPUs

Package: libx265-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 libx265-215 (= ${binary:Version})
Suggests:
 libx265-doc
Description: H.265/HEVC video stream encoder (development files)
 libx265 is an encoding library for creating H.265/High Efficiency Video Coding
 (HEVC) video streams.
 .
 This is the development package which contains headers and libraries for
 libx265.

Package: libx265-215
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: H.265/HEVC video stream encoder (shared library)
 libx265 is an encoding library for creating H.265/High Efficiency Video Coding
 (HEVC) video streams.
 .
 This package contains the shared library.

Package: libx265-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Description: H.265/HEVC video stream encoder (documentation)
 libx265 is an encoding library for creating H.265/High Efficiency Video Coding
 (HEVC) video streams.
 .
 This package contains documentation for the command line encoder and the
 shared library.
